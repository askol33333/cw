import React from "react";
import {getMonth} from "../../methods"



const Title=()=>{
    return (
        <header>
            Список коментарів за {new Date().getDate()} {getMonth()}
        </header>
    )
}

export default Title;
